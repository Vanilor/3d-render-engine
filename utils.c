#include "utils.h"


Timer* CreateTimer()
{
	Timer* timer = NULL;

	timer = (Timer*)calloc(1, sizeof(Timer));
	if (!timer)
		return NULL;

	timer->startTime = -1.0;
	timer->currentTime = 0.0;
	timer->previousTime = timer->currentTime;
	timer->delta = 0.0;

	return timer;
}

void FreeTimer(Timer* timer)
{
	free(timer);
}

void StartTimer(Timer* timer)
{
	if (!timer)
		return;

	timer->startTime = SDL_GetTicks() / 1000.0;
	timer->currentTime = 0.0;
	timer->previousTime = 0.0;
	timer->delta = 0.0;
}

void UpdateTimer(Timer* timer)
{
	timer->previousTime = timer->currentTime;
	timer->currentTime = SDL_GetTicks() / 1000.0 - timer->startTime;
	timer->delta = timer->currentTime - timer->previousTime;
}

char * getLine(FILE* p_file)
{
	char* line = NULL;
	long position;
	int size = 0;
	char c;

	if (!p_file)
		return line;

	position = ftell(p_file);

	do
	{
		if (!fread(&c, sizeof(char), 1, p_file))
			break;
		size++;
	} while (c != '\n' && c != EOF);

//	while ((c = fgetc(p_file)) != EOF && (c = fgetc(p_file)) != '\n')
//		size++;

	if (!size)
		return line;

	if (c == '\n')
		size--;

	line = (char*)calloc(size + 1, sizeof(char));

	if (!line)
		return line;

	fseek(p_file, position, SEEK_SET);
	fread(line, sizeof(char), size, p_file);
	fread(&c, sizeof(char), 1, p_file);

	return line;
}

void _SDL_SetPixel(SDL_Surface* p_affichage, int p_x, int p_y, Uint32 p_coul)
{
	// test si le pixel est dans la fen�tre avant de le dessiner si besoin
	if ((p_x >= 0 && p_x < p_affichage->clip_rect.w) && (p_y >= 0 && p_y < p_affichage->clip_rect.h))* ((Uint32*)(p_affichage->pixels) + p_x + p_y * p_affichage->w) = p_coul;
}

void _SDL_DrawLine(SDL_Surface* p_affichage, int p_x1, int p_y1, int p_x2, int p_y2, const Uint8 p_red, const Uint8 p_green, const Uint8 p_blue)
{
	int d, dx, dy, aincr, bincr, xincr, yincr, x, y;
	Uint32 coul = SDL_MapRGB(p_affichage->format, p_red, p_green, p_blue);
	// On regarde l'inclinaison de la ligne pour savoir si une abscisse a plusieurs ordonn�es ou l'inverse

	// Angle > 45�
	if (abs(p_x2 - p_x1) < abs(p_y2 - p_y1)) {
		/* parcours par l'axe vertical */


		if (p_y1 > p_y2) {
			int temp;
			temp = p_x1;
			p_x1 = p_x2;
			p_x2 = temp;

			temp = p_y1;
			p_y1 = p_y2;
			p_y2 = temp;
		}

		xincr = p_x2 > p_x1 ? 1 : -1;
		dy = p_y2 - p_y1;
		dx = abs(p_x2 - p_x1);
		d = 2 * dx - dy;
		aincr = 2 * (dx - dy);
		bincr = 2 * dx;
		x = p_x1;
		y = p_y1;

		_SDL_SetPixel(p_affichage, x, y, coul);

		for (y = p_y1 + 1; y <= p_y2; ++y) {
			if (d >= 0) {
				x += xincr;
				d += aincr;
			}
			else
				d += bincr;

			_SDL_SetPixel(p_affichage, x, y, coul);
		}

		// Angle < 45�
	}
	else {
		/* parcours par l'axe horizontal */

		if (p_x1 > p_x2) {
			int temp;
			temp = p_x1;
			p_x1 = p_x2;
			p_x2 = temp;

			temp = p_y1;
			p_y1 = p_y2;
			p_y2 = temp;
		}

		yincr = p_y2 > p_y1 ? 1 : -1;
		dx = p_x2 - p_x1;
		dy = abs(p_y2 - p_y1);
		d = 2 * dy - dx;
		aincr = 2 * (dy - dx);
		bincr = 2 * dy;
		x = p_x1;
		y = p_y1;

		_SDL_SetPixel(p_affichage, x, y, coul);

		for (x = p_x1 + 1; x <= p_x2; ++x) {
			if (d >= 0) {
				y += yincr;
				d += aincr;
			}
			else
				d += bincr;

			_SDL_SetPixel(p_affichage, x, y, coul);
		}
	}
}
