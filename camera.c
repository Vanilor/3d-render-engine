#include "camera.h"

Camera* CreateCamera(double r, double l, double t, double b, double n, double f, int x, int y)
{
	Camera* cam = NULL;

	cam = (Camera*)calloc(1, sizeof(Camera));
	if (!cam)
		return cam;

	cam->m_bottom	= b;
	cam->m_top		= t;
	cam->m_left		= l;
	cam->m_right	= r;
	cam->m_near		= n;
	cam->m_far		= f;
	
	cam->m_resX		= x;
	cam->m_resY		= y;

	cam->m_projMatrix = GetProjectionMatrix(cam);

	return cam;
}

void RenderImage(Camera* p_cam, Object* p_obj, SDL_Surface* p_surface)
{
	Mat4 m = Identity4(), m1;
	unsigned int i;

	if (!p_cam || !p_obj || !p_surface)
		return;

	if (p_obj->m_mesh)
	{
		Vec4 begin, end;
		Vec3 beginProj, endProj;
		int color;

		m = MulMat4(GetInverseModelMatrix(p_cam->m_obj), GetModelMatrix(p_obj));
		m = MulMat4(p_cam->m_projMatrix, m);

		for (i = 0; i < p_obj->m_mesh->m_nbEdges; i++)
		{
			int x1, y1;
			int x2, y2;
			int indexBegin, indexEnd;

			indexBegin = p_obj->m_mesh->m_edges[i].m_indexBegin;
			indexEnd   = p_obj->m_mesh->m_edges[i].m_indexEnd;

			begin = MulMat4Vec4(m, p_obj->m_mesh->m_vertices[indexBegin]);
			end   = MulMat4Vec4(m, p_obj->m_mesh->m_vertices[indexEnd]);

			beginProj = Vec4to3(begin);
			endProj   = Vec4to3(end);

			if (beginProj.z < -1.0 || endProj.z < -1.0 || beginProj.z > 1.0 || endProj.z > 1.0)
				continue;

			x1 = (int)((beginProj.x + 1.0) / 2.0 * p_cam->m_resX);
			y1 = (int)((beginProj.y + 1.0) / 2.0 * p_cam->m_resY);
			y1 = p_cam->m_resY - y1;

			x2 = (int)((endProj.x + 1.0) / 2.0 * p_cam->m_resX);
			y2 = (int)((endProj.y + 1.0) / 2.0 * p_cam->m_resY);
			y2 = p_cam->m_resY - y2;

			color = (int)((beginProj.z + 1.0) / 2.0 * 255);

			_SDL_DrawLine(p_surface, x1, y1, x2, y2, 255 - color, 64, color);
		}
	}

	for (i = 0; i < p_obj->m_nbChildren; i++)
		RenderImage(p_cam, p_obj->m_children[i], p_surface);
}

Mat4 GetProjectionMatrix(Camera *p_cam)
{
	Mat4 projectionMatrix = Identity4();

	if (p_cam->m_right == p_cam->m_left || p_cam->m_top == p_cam->m_bottom || p_cam->m_far == p_cam->m_near)
		return projectionMatrix;

	//TODO: Test this matrix
	projectionMatrix.data[0][0] = 2.0 * p_cam->m_near / (p_cam->m_right - p_cam->m_left);
	projectionMatrix.data[1][1] = 2.0 * p_cam->m_near / (p_cam->m_top - p_cam->m_bottom);
	projectionMatrix.data[2][2] = (p_cam->m_far + p_cam->m_near) / (p_cam->m_far - p_cam->m_near);
	projectionMatrix.data[3][3] = 0.0;

	projectionMatrix.data[0][2] = -(p_cam->m_right + p_cam->m_left) / (p_cam->m_right - p_cam->m_left);
	projectionMatrix.data[1][2] = -(p_cam->m_top + p_cam->m_bottom) / (p_cam->m_top - p_cam->m_bottom);
	projectionMatrix.data[3][2] = 1.0;

	projectionMatrix.data[2][3] = -2.0 * p_cam->m_far * p_cam->m_near / (p_cam->m_far - p_cam->m_near);

	return projectionMatrix;
}
