#include "object.h"

Object* CreateObject(Mat4 p_localTransform, Object* p_parent)
{
	Object* result;
	result = (Object*)calloc(1, sizeof(Object));

	if (!result)
		return NULL;

	result->m_localTransform	= p_localTransform;
	result->m_parent			= NULL;
	result->m_mesh				= NULL;
	result->m_children			= NULL;
	result->m_nbChildren		= 0;
	result->m_nbChildrenMax		= 0;

	SetParent(result, p_parent);

	return result;
}

void AddChild(Object* p_obj, Object* p_child)
{
	size_t i;

	if (!p_obj || !p_child)
		return;

	// On ne peux pas �tre son propre enfant
	if (p_obj == p_child)
		return;

	// V�rification que l'objet n'est pas d�j� parmi les enfants
	for (i = 0; i < p_obj->m_nbChildren; i++)
	{
		if (p_child == p_obj->m_children[i])
			return;
	}

	// Modification de l'espace n�cessaire pour stocker les enfants si n�cessaire
	if (p_obj->m_nbChildren >= p_obj->m_nbChildrenMax)
	{
		Object** newMemorySpace;

		if(p_obj->m_nbChildrenMax)
			p_obj->m_nbChildrenMax *= 2;
		else
			p_obj->m_nbChildrenMax = 1;

		newMemorySpace = (Object **)realloc(p_obj->m_children, p_obj->m_nbChildrenMax * sizeof(Object*));

		// On test si l'allocation a fonctionn�
		if (!newMemorySpace)
			return;

		p_obj->m_children = newMemorySpace;
	}

	// On ajoute p_child dans la liste des enfants
	p_obj->m_children[p_obj->m_nbChildren] = p_child;
	p_obj->m_nbChildren++;
}

void RemoveChild(Object* p_obj, Object* p_child)
{
	size_t i, j;

	if (!p_obj || !p_child)
		return;

	for (i = 0; i < p_obj->m_nbChildren; i++)
	{
		if (p_obj->m_children[i] == p_child)
		{
			for (j = i; j < p_obj->m_nbChildren-1; j++)
				p_obj->m_children[j] = p_obj->m_children[j + 1];
			p_obj->m_children[j] = NULL;
			p_obj->m_nbChildren--;

			return;
		}
	}
}

void SetParent(Object* p_obj, Object* p_parent)
{
	Mat4 transformMatrix;

	if (!p_obj)
		return;

	// On v�rifie que p_parent ne soit pas l'objet ni qu'il soit d�j� son parent
	if (p_obj == p_parent || p_obj->m_parent == p_parent)
		return;

	//TODO: Modification de la matrice A TESTER
	transformMatrix = GetMatrix(p_obj, p_parent);
	p_obj->m_localTransform = transformMatrix;

	//Retrait du noeud p_child de la liste des enfants de son ancien parent
	if (p_obj->m_parent)
		RemoveChild(p_obj->m_parent, p_obj);

	// Ajout du parent
	p_obj->m_parent = p_parent;

	// On ajoute p_obj dans la liste des enfants de p_parent
	AddChild(p_parent, p_obj);

}

Object* GetParent(Object* p_obj)
{
	if (!p_obj)
		return NULL;

	return p_obj->m_parent;
}

void SetMesh(Object* p_obj, Mesh* p_mesh)
{
	if (p_obj->m_mesh)
		free(p_obj->m_mesh);

	p_obj->m_mesh = p_mesh;
}

void SetMatrix(Object* p_obj, Object* p_ref, Mat4 p_matrix)
{
	if (!p_obj)
		return;

	// Si on ne change pas de r�f�rentiel, on applique la nouvelle matrice � la matrice locale
	if (p_ref == p_obj->m_parent)
		p_obj->m_localTransform = p_matrix;
	else
	{
		// Sinon, on trouve la matrice de passage de notre r�f�rentiel vers le nouveau
		Mat4 refModelMatrix = GetModelMatrix(p_ref);
		Mat4 parentModelMatrix = GetInverseModelMatrix(p_obj->m_parent);

		p_obj->m_localTransform = MulMat4(MulMat4(parentModelMatrix, refModelMatrix), p_matrix);
	}
}

Mat4 GetMatrix(Object* p_obj, Object* p_ref)
{
	Mat4 matrix = Identity4();

	if (!p_obj || p_obj == p_ref)
		return matrix;

	// On r�cup�re la matrice de transformation locale
	matrix = p_obj->m_localTransform;
	p_obj = p_obj->m_parent;

	// On parcours les parents jusqu'� arriver � NULL (le r�f�renciel monde) o� au r�f�rentiel voulu
	while (p_obj && p_obj != p_ref)
	{
		matrix = MulMat4(p_obj->m_localTransform, matrix);
		p_obj = p_obj->m_parent;
	}

	// Si on a atteind le r�f�renciel d�sir�, on renvoie la matrice ...
	if (p_obj == p_ref)
		return matrix;

	// ... sinon, on calcul la matrice model inverse de l'objet r�f�rentiel que l'on multiplie � notre matrice
	return MulMat4(GetInverseModelMatrix(p_ref), matrix);
}

Mat4 GetModelMatrix(Object* p_obj)
{
	Mat4 result = GetMatrix(p_obj, NULL);

	return result;
}

Mat4 GetInverseModelMatrix(Object* p_obj)
{
	Mat4 result = GetModelMatrix(p_obj);

	return Inverse4(result);
}

void Translate(Object* p_obj, Vec3 p_vector, Object* p_ref)
{
	Mat4 m = GetMatrix(p_obj, p_ref);

	MulMat4(m, SetTranslationMatrix(p_vector));

	SetMatrix(p_obj, p_ref, m);
}

void XRotate(Object* p_obj, double angle, Object* p_ref)
{
	Mat4 m = GetMatrix(p_obj, p_ref);

	angle = M_PI / 180;

	m = MulMat4(m, SetRotationXMatrix(angle));

	SetMatrix(p_obj, p_ref, m);
}

void YRotate(Object* p_obj, double angle, Object* p_ref)
{

	angle *= M_PI / 180;

	Mat4 m = GetMatrix(p_obj, p_ref);

	m = MulMat4(m, SetRotationYMatrix(angle));

	SetMatrix(p_obj, p_ref, m);
}

void ZRotate(Object* p_obj, double angle, Object* p_ref)
{

	angle = M_PI / 180;

	Mat4 m = GetMatrix(p_obj, p_ref);

	m = MulMat4(m, SetRotationZMatrix(angle));

	SetMatrix(p_obj, p_ref, m);
}

void Scale(Object* p_obj, Vec3 p_vector, Object* p_ref)
{
	Mat4 m = GetMatrix(p_obj, p_ref);

	MulMat4(m, SetScaleMatrix(p_vector));

	SetMatrix(p_obj, p_ref, m);
}

void LocalTranslate(Object* p_obj, Vec3 p_vector)
{
	Translate(p_obj, p_vector, p_obj->m_parent);
}

void LocalXRotate(Object* p_obj, double angle)
{
	XRotate(p_obj, angle, p_obj->m_parent);
}

void LocalYRotate(Object* p_obj, double angle)
{
	YRotate(p_obj, angle, p_obj->m_parent);
}

void LocalZRotate(Object* p_obj, double angle)
{
	ZRotate(p_obj, angle, p_obj->m_parent);
}

void LocalScale(Object* p_obj, Vec3 p_vector)
{
	Scale(p_obj, p_vector, p_obj->m_parent);
}
