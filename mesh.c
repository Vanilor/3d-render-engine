#include "mesh.h"

Mesh* LoadMeshFromFile(const char* p_path)
{
	Mesh* mesh = NULL;
	FILE* objFile;

	int nbVertices = 0, nbLines = 0;
	char* context = NULL;
	char* line = NULL;

	fopen_s(&objFile, p_path, "r");

	if (!objFile)
		return mesh;

	while (line = getLine(objFile))
	{
		char *word;

		word = strtok_s(line, " ", &context);
		if (!word)
			continue;

		if (!strcmp(word, "v"))
			nbVertices++;

		if (!strcmp(word, "f"))
		{
			while (word = strtok_s(NULL, " ", &context))
				nbLines++;
		}
	}

	mesh = (Mesh*)calloc(1, sizeof(Mesh));
	if (!mesh)
		return mesh;

	mesh->m_nbVertices = nbVertices;
	mesh->m_vertices = (Vec4*)calloc(nbVertices, sizeof(Vec4));

	mesh->m_nbEdges = nbLines;
	mesh->m_edges = (Line *)calloc(nbLines, sizeof(Line));

	if (!mesh->m_vertices || !mesh->m_edges)
	{
		free(mesh->m_vertices);
		free(mesh->m_edges);
		free(mesh);

		return NULL;
	}

	fseek(objFile, 0, SEEK_SET);
	nbVertices = 0;
	nbLines = 0;

	while (line = getLine(objFile))
	{
		char *word;

		word = strtok_s(line, " ", &context);
		if (!word)
			continue;

		if (!strncmp(word, "v", 2))
		{
			int i = 0;
			while (word = strtok_s(NULL, " ", &context))
			{
				mesh->m_vertices[nbVertices].data[i] = atof(word);
				i++;
			}
			mesh->m_vertices[nbVertices].w = 1.0;
			nbVertices++;
		}
		else if (!strncmp(word, "f", 2))
		{
			int firstIndex, index;

			word = strtok_s(NULL, " ", &context);
			firstIndex = atoi(word);

			mesh->m_edges[nbLines].m_indexBegin = firstIndex - 1;

			while (word = strtok_s(NULL, " ", &context))
			{
				index = atoi(word);
				mesh->m_edges[nbLines].m_indexEnd = index - 1;
				nbLines++;
				mesh->m_edges[nbLines].m_indexBegin = index - 1;
			}

			mesh->m_edges[nbLines].m_indexEnd = firstIndex - 1;
			nbLines++;
		}
	}

	return mesh;
}
