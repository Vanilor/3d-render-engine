#pragma once

#include "object.h"

typedef struct sOrbit {

	double perihelion;
	double aphelion;
	double semiMajorAxis;
	double semiMinorAxis;
	double semiLactusRectum;
	double translationVelocity;
	double instantVelocity;
	double angularVelocity;
	double angle;
	double radius;
	double excentricity;
	unsigned long long specificOrbitalEnergy;
	double standardGravitationalParam;
	unsigned long long specificAngularMomentum;
	double angularMomentum;
	double inclinaison;
	double period; //Assumed as a constant, in DAYS

}orbit;

typedef struct sAstroObj astroObj; 

struct sAstroObj {

	Object* obj;
	Mat4 initLocalTransform;
	astroObj* ref;
	long double mass;
	double radius;
	Vec3* translation;
	orbit* orbt;

};

astroObj* createAstroObj(Object* obj, astroObj* ref, long double mass, double radius, double period, double orbitExcentricity);
Vec3* getLocalTranslation(Mat4 localTransform);
orbit* createOrbit(astroObj* corpse, double period, double e);
void updateAstro(astroObj* obj, long frames);
void displayUIHeader();
void displayAstroParameters(astroObj* obj, Timer* t, long frames);
void printOrbitalParam(double p);