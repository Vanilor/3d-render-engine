#pragma once

#include "Maths/vector4.h"
#include "utils.h"

typedef struct sLine
{
	int		m_indexBegin;
	int		m_indexEnd;
}Line;

typedef struct sMesh
{
	unsigned int	m_nbVertices;
	Vec4 *			m_vertices;

	unsigned int	m_nbEdges;
	Line *			m_edges;
}Mesh;

Mesh* LoadMeshFromFile(const char* p_path);
