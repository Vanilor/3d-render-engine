#pragma once

#include "object.h"
#include "Maths/matrix.h"
#include <math.h>

typedef struct sEllipsoid {

	Vec3 center;
	double a;
	double b;
	double c;
	double e;
	double rmin;
	double rmax;

}ellipsoid;

typedef struct sSphericalCoordinates {

	double r;
	double phi;
	double theta;

}sphericalCoordinates;

/**
*
*	RESEARCHES TRACE 
*
*
ellipsoid createEllipsoid(float semiMajorAxis, float semiMinorAxis, float semiDepth);
void setEllipsoidCenter(ellipsoid* el, Vec3 center);
Vec3 sphericalCoordsToVec(float theta, float phi, ellipsoid el);
Mat4 getEllipsoidRotationMatrix(Vec3 source, Vec3 dest, double angle, ellipsoid el);
sphericalCoordinates getSphericalCoordinates(Vec3 v);
Vec3 EllipticalVectorProduct(Vec3 v1, Vec3 v2, ellipsoid el);
Mat3 getSkewSymetric(Vec3 v, ellipsoid el);
void EllipsoidalXRotationMatrix(Object* obj, double angle, Object* ref);
Vec3 getPosition(Mat4 localTransform);
*/