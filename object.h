#pragma once

#include "Maths/transformation.h"
#include "mesh.h"

typedef struct sObject Object;

/**
 *	@brief	Structure Object
 *	
 *	Structure mod�lisant un objet de la sc�ne
 */
struct sObject
{
	Mat4			m_localTransform;	/**<	Matrice model locale de l'objet		*/
	Mesh *			m_mesh;				/**<	Mesh of the object					*/

	Object *		m_parent;			/**<	Parent de l'objet					*/

	Object **		m_children;			/**<	Liste des enfants de l'objet		*/
	unsigned int	m_nbChildren;		/**<	Nombre d'enfants de l'objet			*/
	unsigned int	m_nbChildrenMax;	/**<	Nombre maximum d'enfants de l'objet	*/
};

/**
 *	@brief	Fonction permettant d'ajouter un enfant � un objet
 *			Cette fonction est appel�e depuis la m�thode SetParent qui permet de changer le parent d'un objet
 *
 *	@see	SetParent
 *
 *	@param	p_obj		Objet auquel on souhaite ajouter un enfant
 *	@param	p_child		Objet � ajouter en enfant
 */
void AddChild(Object* p_obj, Object* p_child);

/**
 *	@brief	Fonction permettant de supprimer un enfant � un objet
 *			Cette fonction est appel�e depuis la m�thode SetParent qui permet de changer le parent d'un objet
 *
 *	@see	SetParent
 *
 *	@param	p_obj		Objet que l'on souhaite retirer de la liste des enfants
 *	@param	p_child		Objet � retirer de la liste des enfants
 */
void RemoveChild(Object* p_obj, Object* p_child);

/**
 *	@brief	Fonction permettant de cr�er un objet
 *
 *	@param	p_localTransform	Matrice model locale de l'objet � cr�er
 *	@param	p_parent			Objet auquel le nouvel objet sera attach�
 *	@return						L'objet cr��
 */
Object* CreateObject(Mat4 p_localTransform, Object* p_parent);

/**
 *	@brief	Fonction permettant de modifier le parent d'un objet
 *			Cette fonction appelle AddChild pour ajouter p_obj dans la liste des enfants de p_parent
 *	
 *	@see	AddChild
 *
 *	@param	p_obj		Objet dont on souhaite modifier le parent
 *	@param	p_parent	Nouveau parent
 */
void SetParent(Object* p_obj, Object* p_parent);

/**
 *	@brief	Fonction permettant de r�cup�rer le parent d'un objet
 *
 *	@param	p_obj	Objet dont on souhaite r�cup�rer le parent
 *	@return			Parent de l'objet pass� en param�tre
 */
Object* GetParent(Object* p_obj);

/**
 *	@brief	Fonction permettant d'ajouter un mesh � un objet
 *
 *	@param	p_obj	Objet sur lequel on souhaite ajouter le mesh
 *	@param	p_mesh	Mesh � ajouter sur l'objet
 */
void SetMesh(Object* p_obj, Mesh* p_mesh);

/**
 *	@brief	Fonction permettant d'appliquer une matrice de transformation sur un objet (p_obj) dans un autre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la translation
 *	@param	p_vector	Vecteur de translation
 */
void SetMatrix(Object* p_obj, Object* p_ref, Mat4 p_matrix);

/**
 *	@brief	Fonction permettant d'obtenir la matrice de transformation d'un objet (p_obj) dans un autre r�f�rentiel (p_ref)
 *
 *	@param	p_obj	Objet dont on souhaite obtenir la matrice de transformation
 *	@param	p_ref	Objet utilis� comme r�f�rentiel pour g�n�rer la matrice de transformation
 *	@return			La matrice de transformation de p_obj dans le r�f�rentiel p_ref
 */
Mat4 GetMatrix(Object* p_obj, Object* p_ref);

/**
 *	@brief	Fonction permettant d'obtenir la matrice de transformation d'un objet (p_obj) dans le r�f�rentiel monde
 *			Cette fonction utilise la fonction GetMatrix avec comme r�f�rentiel NULL
 *
 *	@see	GetMatrix
 *
 *	@param	p_obj	Objet dont on souhaite obtenir la matrice de transformation
 *	@return			La matrice de transformation de p_obj dans le r�f�rentiel monde
 */
Mat4 GetModelMatrix(Object* p_obj);

/**
 *	@brief	Fonction permettant d'obtenir la matrice de transformation inverse d'un objet (p_obj) dans le r�f�rentiel monde
 *			Cette fonction utilise la fonction GetModelMatrix et inverse le r�sultat
 *
 *	@see	GetModelMatrix
 *
 *	@param	p_obj	Objet dont on souhaite obtenir la matrice de transformation inverse
 *	@return			La matrice de transformation inverse de p_obj dans le r�f�rentiel monde
 */
Mat4 GetInverseModelMatrix(Object* p_obj);

//TODO: LocalTranslate, Translate, LocalRotate, Rotate, LocalScale, Scale

/**
 *	@brief	Fonction permettant d'appliquer une translation sur un objet (p_obj) dans son propre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la translation
 *	@param	p_vector	Vecteur de translation
 */
void LocalTranslate(Object* p_obj, Vec3 p_vector);

/**
 *	@brief	Fonction permettant d'appliquer une translation sur un objet (p_obj) dans un autre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la translation
 *	@param	p_vector	Vecteur de translation
 *	@param	p_ref		R�f�rentiel dans lequel on souhaite appliquer la translation
 */
void Translate(Object* p_obj, Vec3 p_vector, Object* p_ref);

/**
 *	@brief	Fonction permettant d'appliquer une rotation autour de l'axe X sur un objet (p_obj) dans son propre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la rotation
 *	@param	p_vector	Matrice de rotation
 */
void LocalXRotate(Object* p_obj, double angle);

/**
 *	@brief	Fonction permettant d'appliquer une rotation autour de l'axe X sur un objet (p_obj) dans un autre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la rotation
 *	@param	p_vector	Matrice de rotation
 *	@param	p_ref		R�f�rentiel dans lequel on souhaite appliquer la rotation
 */
void XRotate(Object* p_obj, double angle, Object* p_ref);

/**
 *	@brief	Fonction permettant d'appliquer une rotation autour de l'axe Y sur un objet (p_obj) dans son propre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la rotation
 *	@param	p_vector	Matrice de rotation
 */
void LocalYRotate(Object* p_obj, double angle);

/**
 *	@brief	Fonction permettant d'appliquer une rotation autour de l'axe Y sur un objet (p_obj) dans un autre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la rotation
 *	@param	p_vector	Matrice de rotation
 *	@param	p_ref		R�f�rentiel dans lequel on souhaite appliquer la rotation
 */
void YRotate(Object* p_obj, double angle, Object* p_ref);

/**
 *	@brief	Fonction permettant d'appliquer une rotation autour de l'axe Z sur un objet (p_obj) dans son propre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la rotation
 *	@param	p_vector	Matrice de rotation
 */
void LocalZRotate(Object* p_obj, double angle);

/**
 *	@brief	Fonction permettant d'appliquer une rotation autour de l'axe Z sur un objet (p_obj) dans un autre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la rotation
 *	@param	p_vector	Matrice de rotation
 *	@param	p_ref		R�f�rentiel dans lequel on souhaite appliquer la rotation
 */
void ZRotate(Object* p_obj, double angle, Object* p_ref);

/**
 *	@brief	Fonction permettant d'appliquer une translation sur un objet (p_obj) dans son propre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la translation
 *	@param	p_vector	Vecteur de translation
 */
void LocalScale(Object* p_obj, Vec3 p_vector);

/**
 *	@brief	Fonction permettant d'appliquer une translation sur un objet (p_obj) dans un autre r�f�rentiel
 *
 *	@param	p_obj		Objet sur lequel on souhaite appliquer la translation
 *	@param	p_vector	Vecteur de translation
 *	@param	p_ref		R�f�rentiel dans lequel on souhaite appliquer la translation
 */
void Scale(Object* p_obj, Vec3 p_vector, Object* p_ref);

