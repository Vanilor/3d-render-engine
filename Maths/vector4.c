#include "vector4.h"

Vec4 Zero4()
{
	Vec4 result = { 0.0, 0.0, 0.0, 0.0 };
	return result;
}
Vec4 Up4()
{
	Vec4 result = { 0.0, 1.0, 0.0, 1.0 };
	return result;
}
Vec4 Down4()
{
	Vec4 result = { 0.0, -1.0, 0.0, 1.0 };
	return result;
}
Vec4 Right4()
{
	Vec4 result = { 1.0, 0.0, 0.0, 1.0 };
	return result;
}
Vec4 Left4()
{
	Vec4 result = { -1.0, 0.0, 0.0, 1.0 };
	return result;
}
Vec4 Front4()
{
	Vec4 result = { 0.0, 0.0, 1.0, 1.0 };
	return result;
}
Vec4 Back4()
{
	Vec4 result = { 0.0, 0.0, -1.0, 1.0 };
	return result;
}

Vec4 Set4(double x, double y, double z, double w)
{
	Vec4 result = { x, y, z, w };
	return result;
}

Vec4 Add4(Vec4 v1, Vec4 v2)
{
	Vec4 result = { v1.x + v2.x, v1.y + v2.y, v1.z + v2.z,  v1.w + v2.w };
	return result;
}

Vec4 MulScal4(Vec4 v, double s)
{
	Vec4 result = { s * v.x, s * v.y, s * v.z, s*v.w };
	return result;
}

double ProdScal4(Vec4 v1, Vec4 v2)
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w;
}

double Norm4(Vec4 v)
{
	return sqrt(ProdScal4(v, v));
}

Vec4 Normalized4(Vec4 v)
{
	return MulScal4(v, 1.0 / Norm4(v));
}

void PrintV4(Vec4 v) {
	printf("%f  ", v.x);
	printf("%f  ", v.y);
	printf("%f  ", v.z);
	printf("%f  ", v.w);
	printf("\n");
}