#include "vector3.h"

Vec3 Zero3()
{
	Vec3 result = { 0.0, 0.0, 0.0 };
	return result;
}
Vec3 Up3()
{
	Vec3 result = { 0.0, 1.0, 0.0 };
	return result;
}
Vec3 Down3()
{
	Vec3 result = { 0.0, -1.0, 0.0 };
	return result;
}
Vec3 Right3()
{
	Vec3 result = { 1.0, 0.0, 0.0 };
	return result;
}
Vec3 Left3()
{
	Vec3 result = { -1.0, 0.0, 0.0 };
	return result;
}
Vec3 Front3()
{
	Vec3 result = { 0.0, 0.0, 1.0 };
	return result;
}
Vec3 Back3()
{
	Vec3 result = { 0.0, 0.0, -1.0 };
	return result;
}

Vec3 Set3(double x, double y, double z)
{
	Vec3 result = { x, y, z };
	return result;
}

Vec3 Add3(Vec3 v1, Vec3 v2)
{
	Vec3 result = { v1.x + v2.x, v1.y + v2.y, v1.z + v2.z };
	return result;
}

Vec3 MulScal3(Vec3 v, double s)
{
	Vec3 result = { s * v.x, s * v.y, s * v.z };
	return result;
}

double ProdScal3(Vec3 v1, Vec3 v2)
{
	double test = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

double Norm3(Vec3 v)
{
	return sqrt(ProdScal3(v, v));
}

Vec3 Normalized3(Vec3 v)
{
	return MulScal3(v, 1.0 / Norm3(v));
}

void PrintV3(Vec3 v) {
	printf("%f  ", v.x);
	printf("%f  ", v.y);
	printf("%f  ", v.z);
	printf("\n");
}