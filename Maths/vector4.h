#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/**
 *	Structure Vec4
 *
 *	Cette structure permet de mod�liser des vecteurs � 4 dimensions (coordonn�es homog�ne).
 *	Il est possible d'acc�der aux donn�es via un tableau data ou via les champs x, y, z et w
 */
typedef union sVec4 {
	struct {
		double x;
		double y;
		double z;
		double w;
	};

	double data[4];
}Vec4;

//////////////////////////
// FONCTIONS RELATIVES AUX VECTEURS � 4 DIMENSIONS
//////////////////////////*

/**
 *	Fonction raccourcie pour d�finir le vecteur nul
 */
Vec4 Zero4();
/**
 *	Fonction raccourcie pour d�finir le vecteur (0, 1, 0, 1)
 */
Vec4 Up4();
/**
 *	Fonction raccourcie pour d�finir le vecteur (0, -1, 0, 1)
 */
Vec4 Down4();
/**
 *	Fonction raccourcie pour d�finir le vecteur (1, 0, 0, 1)
 */
Vec4 Right4();
/**
 *	Fonction raccourcie pour d�finir le vecteur (-1, 0, 0, 1)
 */
Vec4 Left4();
/**
 *	Fonction raccourcie pour d�finir le vecteur (0, 0, 1, 1)
 */
Vec4 Front4();
/**
 *	Fonction raccourcie pour d�finir le vecteur (0, 0, -1, 1)
 */
Vec4 Back4();

/**
 *	Cr�ation d'un vecteur
 */
Vec4 Set4(double x, double y, double z, double w);

/**
 *	Somme de vecteur
 */
Vec4 Add4(Vec4 v1, Vec4 v2);

/**
 *	Multiplication par un scalaire
 */
Vec4 MulScal4(Vec4 v, double s);

/**
 *	Produit scalaire
 */
double ProdScal4(Vec4 v1, Vec4 v2);

/**
 *	Norme du vecteur
 */
double Norm4(Vec4 v);

/**
 *	Vecteur normalis�
 */
Vec4 Normalized4(Vec4 v);

void PrintV4(Vec4 v);
