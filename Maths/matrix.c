#include "matrix.h"
#include "matrix3x3.h"
#include "matrix4x4.h"
#include <stdio.h>

Mat3 Mat3FromMat4(Mat4 m)
{
	Mat3 result;
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			result.data[i][j] = m.data[i][j];
	return result;
}

Mat4 Mat3ToMat4(Mat3 m)
{
	Mat4 result = { 0 };
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			result.data[i][j] = m.data[i][j];
	
	result.data[3][3] = 1;
	return result;
}