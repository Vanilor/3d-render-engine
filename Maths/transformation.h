#pragma once

#include "matrix.h"

#ifndef M_PI
	#define M_PI       3.14159265358979323846   // pi
#endif

Mat4 GetScaleFromMatrix(Mat4 m);

Mat4 GetOrientationFromMatrix(Mat4 m);

Mat4 GetTranslationFromMatrix(Mat4 m);

Mat4 SetScaleMatrix(Vec3 v);

Mat4 SetTranslationMatrix(Vec3 v);

Mat4 SetRotationXMatrix(double angle);
   
Mat4 SetRotationYMatrix(double angle);
   
Mat4 SetRotationZMatrix(double angle);
