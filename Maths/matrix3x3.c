#include "matrix3x3.h"
#include "vector3.h"
#include <stdlib.h>

Mat3 Identity3()
{
	Mat3 result;
	size_t i, j;

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			if (i != j)
				result.data[i][j] = 0;
			else
				result.data[i][j] = 1;
		}
	}

	return result;
}

Mat3 AddMat3(Mat3 m1, Mat3 m2) {

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			m1.data[i][j] += m2.data[i][j];

	return m1;

}

Mat3 Transpose3(Mat3 matrix) {
	Mat3 result = { 0 };
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++)
			result.data[j][i] = matrix.data[i][j];

		return result;
	}
}

Mat3 MulMat3(Mat3 mat1, Mat3 mat2){
	Mat3 result = { 0 };

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			for (int k = 0; k < 3; k++)
				result.data[i][j] += mat1.data[i][k] * mat2.data[k][j];

	return result;
}

Vec3 MulMat3Vec3(Mat3 m, Vec3 v)
{
	Vec3 result = Zero3();
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			result.data[i] += m.data[i][j] * v.data[j];
	return result;
}

Mat3 MulMat3Scal(Mat3 m, double s)
{
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			m.data[i][j] = s * m.data[i][j];
	return m;
}

Vec3 GetLine3(Mat3 m, unsigned int p_index)
{
	Vec3 result = Set3(m.data[p_index][0], m.data[p_index][1], m.data[p_index][2]);
	return result;
	
}

Vec3 GetColumn3(Mat3 m, unsigned int p_index)
{
	Vec3 result = Set3(m.data[0][p_index], m.data[1][p_index], m.data[2][p_index]);
	return result;
}

void SetColumn3(Mat3* m, Vec3 p_column, unsigned int p_index)
{
	for (int i = 0; i < 3; i++)
		m->data[i][p_index] = p_column.data[i];
	return;
}

double Determinant3(Mat3 m)
{
	return 
		+ m.data[0][0] * (m.data[1][1] * m.data[2][2] - m.data[2][1] * m.data[1][2])
		- m.data[1][0] * (m.data[0][1] * m.data[2][2] - m.data[2][1] * m.data[0][2])
		+ m.data[2][0] * (m.data[0][1] * m.data[1][2] - m.data[1][1] * m.data[0][2]);
}

void PrintMat3(Mat3 m)
{
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++)
			printf("%f  ", m.data[i][j]);
		printf("\n");
	}

	return;

}