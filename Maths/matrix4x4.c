#include "matrix.h"
#include "vector4.h"

Mat4 Identity4()
{
	Mat4 result;
	size_t i, j;

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			if (i != j)
				result.data[i][j] = 0;
			else
				result.data[i][j] = 1;
		}
	}

	return result;
}

Mat4 Transpose4(Mat4 matrix)
{
	Mat4 result = { 0 };
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			result.data[j][i] = matrix.data[i][j];

	return result;
}

Mat4 AddMat4(Mat4 mat1, Mat4 mat2)
{
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			mat1.data[i][j] += mat2.data[i][j];

	return mat1;
}

Mat4 MulMat4(Mat4 mat1, Mat4 mat2)
{
	Mat4 result = { 0 };

	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			for (int k = 0; k < 4; k++)
				result.data[i][j] += mat1.data[i][k] * mat2.data[k][j];

	return result;
}

Vec4 MulMat4Vec4(Mat4 m, Vec4 v)
{
	Vec4 result = Zero4();
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			result.data[i] += m.data[i][j] * v.data[j];
	return result;
}

Mat4 MulMat4Scal(Mat4 m, double s)
{
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			m.data[i][j] = s * m.data[i][j];
	return m;
}

Vec4 GetLine4(Mat4 m, unsigned int p_index)
{
	Vec4 result = Set4(m.data[p_index][0], m.data[p_index][1], m.data[p_index][2], m.data[p_index][3]);
	return result;
}

Vec4 GetColumn4(Mat4 m, unsigned int p_index)
{
	Vec4 result = Set4(m.data[0][p_index], m.data[1][p_index], m.data[2][p_index], m.data[3][p_index]);
	return result;
}

void SetColumn4(Mat4* m, Vec4 p_column, unsigned int p_index)
{
	for (int i = 0; i < 4; i++)
		m->data[i][p_index] = p_column.data[i];
	return m;
}

double Determinant4(Mat4 m)
{
	double SubFactor00 = m.data[2][2] * m.data[3][3] - m.data[3][2] * m.data[2][3];
	double SubFactor01 = m.data[2][1] * m.data[3][3] - m.data[3][1] * m.data[2][3];
	double SubFactor02 = m.data[2][1] * m.data[3][2] - m.data[3][1] * m.data[2][2];
	double SubFactor03 = m.data[2][0] * m.data[3][3] - m.data[3][0] * m.data[2][3];
	double SubFactor04 = m.data[2][0] * m.data[3][2] - m.data[3][0] * m.data[2][2];
	double SubFactor05 = m.data[2][0] * m.data[3][1] - m.data[3][0] * m.data[2][1];

	Vec4 DetCof = Set4(
		+(m.data[1][1] * SubFactor00 - m.data[1][2] * SubFactor01 + m.data[1][3] * SubFactor02),
		-(m.data[1][0] * SubFactor00 - m.data[1][2] * SubFactor03 + m.data[1][3] * SubFactor04),
		+(m.data[1][0] * SubFactor01 - m.data[1][1] * SubFactor03 + m.data[1][3] * SubFactor05),
		-(m.data[1][0] * SubFactor02 - m.data[1][1] * SubFactor04 + m.data[1][2] * SubFactor05));

	return
		m.data[0][0] * DetCof.data[0] + m.data[0][1] * DetCof.data[1] +
		m.data[0][2] * DetCof.data[2] + m.data[0][3] * DetCof.data[3];
}

Mat4 Cofactor(Mat4 m, double det)
{
	Mat4 b = Identity4(), result;
	int l, h, o, k, i, j;

	for (h = 0; h < 4; h++)
	{
		for (l = 0; l < 4; l++)
		{
			o = 0;
			k = 0;
			for (i = 0; i < 4; i++)
			{
				for (j = 0; j < 4; j++)
				{
					if (i != h && j != l)
					{
						b.data[o][k] = m.data[i][j];
						if (k < (4 - 2))
							k++;
						else
						{
							k = 0;
							o++;
						}
					}
				}
			}
			result.data[h][l] = pow(-1.0, (double)h + (double)l) * Determinant3(Mat3FromMat4(b));
		}
	}

	return MulMat4Scal(Transpose4(result), 1.0 / det);
}

Mat4 Inverse4(Mat4 m)
{
	double det = Determinant4(m);

	if (det == 0)
	{
		printf("\nInverse of Entered Matrix is not possible\n");
		return Identity4();
	}
	else
		return Cofactor(m, det);
}

void PrintMat4(Mat4 m)
{
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++)
			printf("%f  ", m.data[i][j]);
		printf("\n");
	}

	return;
}