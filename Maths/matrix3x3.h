#pragma once

#include "vector3.h"

typedef union sMat3
{
	Vec3	lines[3];
	double	data[3][3];
}Mat3;

Mat3 Identity3();

Mat3 Transpose3(Mat3 matrix);

Mat3 AddMat3(Mat3 m1, Mat3 m2);

Mat3 MulMat3(Mat3 mat1, Mat3 mat2);

Vec3 MulMat3Vec3(Mat3 m, Vec3 v);

Mat3 MulMat3Scal(Mat3 m, double s);

Vec3 GetLine3(Mat3 m, unsigned int p_index);

Vec3 GetColumn3(Mat3 m, unsigned int p_index);

void SetColumn3(Mat3* m, Vec3 p_column, unsigned int p_index);

double Determinant3(Mat3 m);

void PrintMat3(Mat3 m);
