#pragma once

#include "vector4.h"

typedef union sMat4
{
	Vec4	lines[4];
	double	data[4][4];
}Mat4;

Mat4 Identity4();

Mat4 Transpose4(Mat4 matrix);

Mat4 AddMat4(Mat4 mat1, Mat4 mat2);

Mat4 MulMat4(Mat4 mat1, Mat4 mat2);

Vec4 MulMat4Vec4(Mat4 m, Vec4 v);

Mat4 MulMat4Scal(Mat4 m, double s);

Vec4 GetLine4(Mat4 m, unsigned int p_index);

Vec4 GetColumn4(Mat4 m, unsigned int p_index);

void SetColumn4(Mat4* m, Vec4 p_column, unsigned int p_index);

double Determinant4(Mat4 m);

Mat4 Inverse4(Mat4 m);

void PrintMat4(Mat4 m);
