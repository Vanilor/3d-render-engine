#include "transformation.h"
#include "matrix.h"
#include "matrix3x3.h"
#include "matrix4x4.h"
#include "vector.h"
#include "vector3.h"
#include "vector4.h"
#include <math.h>

Mat4 GetScaleFromMatrix(Mat4 m)
{
	Mat4 scaleMatrix = Identity4();
	Mat3 mat = Mat3FromMat4(m);
	size_t i;

	Vec3 scaleVector = Set3(Norm3(GetColumn3(mat, 0)), Norm3(GetColumn3(mat, 1)), Norm3(GetColumn3(mat, 2)));

	if (Determinant4(m) < 0.0)
		scaleVector = MulScal3(scaleVector, -1.0);

	for (i = 0; i < 3; i++)
		scaleMatrix.data[i][i] *= scaleVector.data[i];

	return scaleMatrix;
}

Mat4 GetOrientationFromMatrix(Mat4 m)
{
	Mat3 subOrientationMatrix = Mat3FromMat4(m);
	Mat4 orientationMatrix;
	size_t i;

	if (Determinant4(m) < 0.0)
	{
		for (i = 0; i < 3; i++)
			SetColumn3(&subOrientationMatrix, MulScal3(Normalized3(GetColumn3(subOrientationMatrix, i)), -1.0), i);
	}
	else
	{
		for (i = 0; i < 3; i++)
			SetColumn3(&subOrientationMatrix, Normalized3(GetColumn3(subOrientationMatrix, i)), i);
	}

	orientationMatrix = Mat3ToMat4(subOrientationMatrix);
	orientationMatrix.lines[3] = m.lines[3];

	return orientationMatrix;
}

Mat4 GetTranslationFromMatrix(Mat4 m)
{
	Mat4 translationMatrix = Identity4();

	SetColumn4(&translationMatrix, GetColumn4(m, 3), 3);

	return translationMatrix;
}

Mat4 SetScaleMatrix(Vec3 v)
{
	Mat3 tmp = { 0 };
	tmp.data[0][2] = v.data[0];
	tmp.data[1][2] = v.data[1];
	tmp.data[2][2] = v.data[2];
	return Mat3ToMat4(tmp);
}

Mat4 SetTranslationMatrix(Vec3 v)
{
	Mat3 tmp = { 0 };
	tmp.data[0][0] = v.data[0];
	tmp.data[1][1] = v.data[1];
	tmp.data[2][2] = v.data[2];
	return Mat3ToMat4(tmp);
}

Mat4 SetRotationXMatrix(double angle)
{
	Mat3 rotM = { 0 };
	rotM.data[1][1] = cos(angle);
	rotM.data[1][2] = (-1) * sin(angle);
	rotM.data[2][1] = sin(angle);
	rotM.data[0][0] = 1;
	Mat4 result = Mat3ToMat4(rotM);
	result.data[2][2] = cos(angle);
	return result;
}

Mat4 SetRotationYMatrix(double angle)
{
	Mat3 rotM = { 0 };
	rotM.data[0][0] = cos(angle);
	rotM.data[2][0] = (-1) * sin(angle);
	rotM.data[0][2] = sin(angle);
	rotM.data[1][1] = 1;
	Mat4 result = Mat3ToMat4(rotM);
	result.data[2][2] = cos(angle);
	return result;
}

Mat4 SetRotationZMatrix(double angle)
{
	Mat3 rotM = { 0 };
	rotM.data[0][0] = cos(angle);
	rotM.data[0][1] = (-1) * sin(angle);
	rotM.data[1][0] = sin(angle);
	rotM.data[1][1] = cos(angle);
	rotM.data[2][2] = 1;
	return Mat3ToMat4(rotM);
}
