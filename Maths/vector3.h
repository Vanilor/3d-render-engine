#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/**
 *	Structure Vec3
 *
 *	Cette structure permet de mod�liser des vecteurs � 3 dimensions (coordon�es carth�siennes).
 *	Il est possible d'acc�der aux donn�es via un tableau data ou via les champs x, y et z
 */
typedef union sVec3 {
	struct {
		double x;
		double y;
		double z;
	};

	double data[3];

}Vec3;

/**
 *	Fonction raccourcie pour d�finir le vecteur nul
 */
Vec3 Zero3();
/**
 *	Fonction raccourcie pour d�finir le vecteur (0, 1, 0)
 */
Vec3 Up3();
/**
 *	Fonction raccourcie pour d�finir le vecteur (0, -1, 0)
 */
Vec3 Down3();
/**
 *	Fonction raccourcie pour d�finir le vecteur (1, 0, 0)
 */
Vec3 Right3();
/**
 *	Fonction raccourcie pour d�finir le vecteur (-1, 0, 0)
 */
Vec3 Left3();
/**
 *	Fonction raccourcie pour d�finir le vecteur (0, 0, 1)
 */
Vec3 Front3();
/**
 *	Fonction raccourcie pour d�finir le vecteur (0, 0, -1)
 */
Vec3 Back3();

/**
 *	Cr�ation d'un vecteur
 */
Vec3 Set3(double x, double y, double z);

/**
 *	Somme de vecteur
 */
Vec3 Add3(Vec3 v1, Vec3 v2);

/**
 *	Multiplication par un scalaire
 */
Vec3 MulScal3(Vec3 v, double s);

/**
 *	Produit scalaire
 */
double ProdScal3(Vec3 v1, Vec3 v2);

/**
 *	Norme du vecteur
 */
double Norm3(Vec3 v);

/**
 *	Vecteur normalis�
 */
Vec3 Normalized3(Vec3 v);

void PrintV3(Vec3 v);