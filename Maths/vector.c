#include "vector.h"
#include "vector3.h"
#include "vector4.h"

Vec4 Vec3to4(Vec3 vector)
{
	Vec4 result = Set4(vector.data[0], vector.data[1], vector.data[2], 1);
	return result;
}

Vec3 Vec4to3(Vec4 vector)
{
	Vec3 result = Set3(vector.data[0], vector.data[1], vector.data[2]);
	return MulScal3(result, 1/vector.w);
}
