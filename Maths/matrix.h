#pragma once

#include "vector.h"

#include "matrix3x3.h"
#include "matrix4x4.h"

Mat3 Mat3FromMat4(Mat4 m);

Mat4 Mat3ToMat4(Mat3 m);