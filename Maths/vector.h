#pragma once

#include "vector3.h"
#include "vector4.h"

/**
 *	Fonction permettant de passer d'un vecteur en coordonnées carthésiennes vers homogènes.
 */
Vec4 Vec3to4(Vec3 vector);

/**
 *	Fonction permettant de passer d'un vecteur en coordonnées homogènes vers carthésiennes.
 */
Vec3 Vec4to3(Vec4 vector);
