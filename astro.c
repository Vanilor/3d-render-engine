#include "astro.h"

astroObj* createAstroObj(Object* obj, astroObj* ref, long double mass, double radius, double period, double orbitExcentricity) {

	astroObj* corpse = calloc(1, sizeof(astroObj));

	if (!corpse)
		return NULL;

	corpse->obj = obj;
	corpse->mass = mass;
	corpse->radius = radius;
	corpse->ref = ref;
	corpse->orbt = createOrbit(corpse, period, orbitExcentricity);

	return corpse;

}

Vec3* getLocalTranslation(Mat4 localTransform) {

	Vec3* result = calloc(1, sizeof(Vec3));
	if (!result)
		return NULL;

	result->x = localTransform.data[3][0];
	result->y = localTransform.data[3][1];
	result->z = localTransform.data[3][2];

	return result;

}

orbit* createOrbit(astroObj* corpse, double period, double e) {

	orbit* result = (orbit*) calloc(1, sizeof(orbit));

	if (!result)
		return NULL;

	const double G = 6.67 * pow(10, -11);

	result->period = period*24*3600;
	result->excentricity = e;

	if(corpse->ref)
		result->standardGravitationalParam = G * (corpse->mass + corpse->ref->mass);
	else 
		result->standardGravitationalParam = G * corpse->mass;

	result->semiMajorAxis = cbrt((result->standardGravitationalParam * pow(result->period, 2)) / (4 * pow(M_PI, 2)));
	result->semiMajorAxis /= 1000; //We work in km instead of m
	result->semiMinorAxis = result->semiMajorAxis * sqrt(1 - pow(result->excentricity, 2));
	result->semiLactusRectum = pow(result->semiMinorAxis, 2) / result->semiMajorAxis;
	result->angle = 0.001;
	result->radius = result->semiLactusRectum / (1 + result->excentricity * cos(result->angle));
	result->specificOrbitalEnergy = result->standardGravitationalParam / (2.0 * result->semiMajorAxis);
	result->translationVelocity = 2 * M_PI * result->semiMajorAxis / result->period; //Need to be updated
	result->specificAngularMomentum = result->standardGravitationalParam * sqrt((1 - pow(result->excentricity, 2)) / (2 * result->specificOrbitalEnergy));
	result->aphelion = result->semiMajorAxis * (1 + result->excentricity);
	result->perihelion = result->semiMajorAxis * (1 - result->excentricity);
	result->angularMomentum = result->specificAngularMomentum * corpse->mass;
	result->angularVelocity = result->translationVelocity / result->radius;

	return result;

}

void updateAstro(astroObj* corpse, long frames) {

	/**
	*	We use empiric correction factors in order to avoid too large numbers
	*	Provided by the astrophysic engine and allow us to display them into the render engine
	*/
	double distanceCorrectionFactor = 1000000;

	orbit* orbt = corpse->orbt;

	/*
	orbt->excentricity = sqrt(1 + 2 * orbt->specificOrbitalEnergy * pow(orbt->specificAngularMomentum, 2) / orbt->standardGravitationalParam);
	orbt->specificOrbitalEnergy = pow(orbt->translationVelocity, 2) / 2 - orbt->standardGravitationalParam / orbt->radius;
	orbt->semiMajorAxis = orbt->standardGravitationalParam / (2 * orbt->specificOrbitalEnergy);
	*/
	orbt->translationVelocity = sqrt(orbt->standardGravitationalParam * (2 / (1000*orbt->radius) - 1 / (1000*orbt->semiMajorAxis)));
	orbt->translationVelocity /= 1000; //We are working in km instead of m

	orbt->angularVelocity = orbt->translationVelocity / orbt->radius;
	orbt->angle = fmod(orbt->angularVelocity * frames, 2 * M_PI);
	orbt->radius = orbt->semiLactusRectum / (1 + orbt->excentricity * cos(orbt->angle));

	Mat4 newMatrix = corpse->initLocalTransform;

	/**
	*	X axis
	*/
	newMatrix.data[0][3] = orbt->radius * sin(orbt->angle) / distanceCorrectionFactor;
	if (abs(newMatrix.data[0][3]) > 20)
		newMatrix.data[0][3] /= 20;

	printf("\n");
	printf("%g \n", newMatrix.data[0][3]);

	/**
	*	Z axis
	*/
	newMatrix.data[2][3] = orbt->radius * cos(orbt->angle) / distanceCorrectionFactor;
	/*if (abs(newMatrix.data[2][3]) > 20)
		newMatrix.data[2][3] /= 20;*/
	printf("%f", newMatrix.data[2][3]);
	printf("\n\n");
	SetMatrix(corpse->obj, corpse->ref->obj, newMatrix);
	return;

}

void displayUIHeader() {

	printf("Constants              ");
	printf("Orbital parameters");

	printf("\n");

	printf("Mass      ");
	printf("Radius    ");
	printf("Aphelion  ");
	printf("P-helion  ");
	printf("Distance  ");
	printf("S-Major   ");
	printf("S-Minor   ");
	printf("S-LacRect ");
	printf("Excent    ");
	printf("Angle     ");
	printf("A-Veloc   ");
	printf("SAM       ");
	printf("T-Vecloc  ");
	printf("SOE       ");

	printf("\n");

	return;
}

void displayAstroParameters(astroObj* obj, Timer* t, long frames) {

	orbit* orbt = obj->orbt;

	printOrbitalParam(obj->mass);
	printOrbitalParam(obj->radius);
	printOrbitalParam(orbt->aphelion);
	printOrbitalParam(orbt->perihelion);
	printOrbitalParam(orbt->radius);
	printOrbitalParam(orbt->semiMajorAxis);
	printOrbitalParam(orbt->semiMinorAxis);
	printOrbitalParam(orbt->semiLactusRectum);
	printOrbitalParam(orbt->excentricity);
	printOrbitalParam(orbt->angle);
	printOrbitalParam(orbt->angularVelocity);
	printOrbitalParam(orbt->specificAngularMomentum);
	printOrbitalParam(orbt->translationVelocity);
	printOrbitalParam(orbt->specificOrbitalEnergy);

	printf("Real time : %li", frames);

	printf("\n");

	return;
}

void printOrbitalParam(double p) {

	if (p < 10.0)
		printf("%.44e  ", p);
	else if (p < 2147483647)
		printf("%li  ", (long)p);
	else
		printf("%Lf  ", (long double)p);
	return;

}