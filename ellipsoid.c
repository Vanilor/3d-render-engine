#include "ellipsoid.h"


/**
*
*	ALL THE ABOVE IS CONSIDERED AS RESEARCHES TRACE BUT DID NOT SUCCEED
*
/**
*
*	SOURCE : 
*	https://www.researchgate.net/publication/291975543_Elliptic_Quaternions_and_Generating_Elliptical_Rotation_Matrices
*
*

ellipsoid createEllipsoid(float semiMajorAxis, float semiMinorAxis, float semiDepth) {

	ellipsoid result = { 0 };
	Vec3 defaultCenter = { 0 };

	result.center = defaultCenter;
	result.a = semiMajorAxis;
	result.b = semiMinorAxis;
	result.c = semiDepth;
	result.e = sqrt(1 - pow((result.b / result.a), 2));
	result.rmax = result.a * (1 + result.e);
	result.rmin = result.a * (1 - result.e);

	return result;

}

void setEllipsoidCenter(ellipsoid* el, Vec3 center) {

	el->center = center;
	return;
}

Vec3 sphericalCoordsToVec(float theta, float phi, ellipsoid el) {

	Vec3 result = { 0 };

	result.x = el.a * cos(theta) * cos(phi);
	result.y = el.b * cos(theta) * sin(phi);
	result.z = el.c * cos(theta);

	return result;

}

Mat4 getEllipsoidRotationMatrix(Vec3 source, Vec3 dest, double angle, ellipsoid el) {

	Mat3 I = Identity3();

	/**
	*	We're using the Rodrigues rotation formula 
	*	Source : 
	*	https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula#Matrix_notation
	*

	Mat3 skewSymetric = getSkewSymetric(source, el);

	Mat3 rotationMatrix = { 0 };
	rotationMatrix = AddMat3(
					MulMat3Scal(
						MulMat3(
							skewSymetric, 
							skewSymetric
						),
						1 - cos(angle)
					),
					AddMat3(I,
						MulMat3Scal(
							skewSymetric,
							sin(angle)
						)
					)
				);

	return Mat3ToMat4(rotationMatrix);

}


sphericalCoordinates getSphericalCoordinates(Vec3 v) {

	sphericalCoordinates coords = { 0 };

	coords.r = sqrt(pow(v.x, 2) + pow(v.y, 2) + pow(v.z, 2));
	coords.phi = atan(v.y / v.x);
	coords.theta = acos(v.z / coords.r);

	return coords;

}

Vec3 EllipticalVectorProduct(Vec3 v1, Vec3 v2, ellipsoid el){

	return MulMat3Vec3(getSkewSymetric(v1, el), v2);

}

Mat3 getSkewSymetric(Vec3 v, ellipsoid el) {

	double delta = sqrt(el.a * el.b * el.c);
	Mat3 result = { 0 };

	result.data[0][1] = (-1) * v.z / el.a;
	result.data[0][2] = v.y / el.a;
	result.data[1][0] = v.z / el.b;
	result.data[1][2] = (-1) * v.x / el.b;
	result.data[2][0] = (-1) * v.y / el.c;
	result.data[2][1] = v.x / el.c;

	return MulMat3Scal(result, delta);

}

void EllipsoidalXRotationMatrix(Object* obj, double angle, Object* ref) {

	Mat4 initialMatrix = GetMatrix(obj, ref);

	Vec3 sourcePosition = getPosition(obj->m_localTransform);
	Vec3 destPosition = MulScal3(sourcePosition, 1.1);

	ellipsoid test = createEllipsoid(2.0, 1.0, 1.0);

	Mat4 thetaMat = getEllipsoidRotationMatrix(sourcePosition, destPosition, angle, test);
	PrintMat4(thetaMat);
	SetMatrix(obj, ref, MulMat4(initialMatrix, thetaMat));
	return;
}

Vec3 getPosition(Mat4 localTransform) {

	Vec3 position = { 0 };

	position.x = localTransform.data[0][3];
	position.y = localTransform.data[1][3];
	position.z = localTransform.data[2][3];

	return position;

}

*/