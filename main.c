#include "camera.h"
#include "object.h"
#include "astro.h"
#include <time.h>
#include "Maths/matrix.h"
#include "ellipsoid.h"

#define WIDTH 850
#define HEIGHT 504

#define rotationSpeed 1

typedef struct sPoint {
	int m_x;
	int m_y;
}point;

int main(int argc, char** argv)
{
	SDL_Surface* window;
	char quit = 0;
	SDL_Event e;
	Timer* timer = NULL;
	Uint8 mouseButton;
	point mousePos;

	char stereo = 0;

	// Initialisation
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "ERREUR - SDL_Init\n>>> %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	// Permet de quitter proprement par la suite (revenir � la r�solution initiale de l'�cran avec le bon format vid�o)
	atexit(SDL_Quit);
	// Initialise le mode vid�o de la fen�tre
	window = SDL_SetVideoMode(WIDTH, HEIGHT, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);

	// Si on n'a pas r�ussi � cr�er la fen�tre on retourne une erreur
	if (window == NULL) {
		fprintf(stderr, "ERREUR - impossible de passer en : %dx%dx%d\n>>> %s\n", WIDTH, HEIGHT, 32, SDL_GetError());
		exit(EXIT_FAILURE);
	}

	// On met le titre sur la fen�tre
	SDL_WM_SetCaption("3D", NULL);


	// Arbre de sc�ne
	Object* root = CreateObject(Identity4(), NULL);

	Camera* cam = CreateCamera(16.0/9.0, -16.0 / 9.0, 1.0, -1.0, -2.0, -50.0, WIDTH, HEIGHT);
	cam->m_obj = CreateObject(Identity4(), root);


	/**
	*
	*	SUN
	*
	**/
	Mat4 sunMat = Identity4();
	sunMat.data[2][3] = -30.0;
	Object* sunFix = CreateObject(sunMat, root);
	Object* sunRot = CreateObject(sunMat, root);

	Object* sun = CreateObject(sunMat, sunFix);
	sun->m_mesh = LoadMeshFromFile("bigSphere.obj");
	astroObj* sunAstro = createAstroObj(sun, NULL, 1.989 * pow(10, 30), 695510, 0, 0);

	/**
	*
	*	MERCURY
	*
	**/
	Mat4 mercuryMat = Identity4();
	mercuryMat.data[2][3] = -28.0;
	mercuryMat.data[0][3] = 2.0;	
	Object* mercuryRot = CreateObject(sunMat, root);
	Object* mercuryFix = CreateObject(mercuryMat, mercuryRot);

	for (int i = 0; i < 3; i++)
		mercuryMat.data[i][i] = 0.3;
	Object* mercury = CreateObject(mercuryMat, mercuryFix);
	mercury->m_mesh = LoadMeshFromFile("bigSphere.obj");

	astroObj* mercuryAstro = createAstroObj(mercury, sunAstro, 3.301 * pow(10, 23), 2439.7, 77, 0.206);

	/**
	*
	*	VENUS
	*
	**/
	Mat4 venusMat = Identity4();
	venusMat.data[2][3] = -26.0;
	venusMat.data[0][3] = 2.0;
	Object* venusRot = CreateObject(sunMat, root);
	Object* venusFix = CreateObject(venusMat, venusRot);

	for (int i = 0; i < 3; i++)
		venusMat.data[i][i] = 0.3;
	Object* venus = CreateObject(venusMat, venusFix);
	venus->m_mesh = LoadMeshFromFile("bigSphere.obj");

	astroObj* venusAstro = createAstroObj(venus, sunAstro, 4.867 * pow(10, 24), 6051.8, 225, 0.007);

	/**
	*
	*	EARTH
	*
	**/
	Mat4 earthMat = Identity4();
	earthMat.data[2][3] = -5.0;
	earthMat.data[0][3] = 2.0;
	//xRot matrixes are used to create rotation to children
	Object* earthRot = CreateObject(sunMat, root);
	//xFix matrixes are fix onto their axes
	Object* earthFix = CreateObject(earthMat, earthRot);

	for (int i = 0; i < 3; i++)
		earthMat.data[i][i] = 0.5;
	Object* earth = CreateObject(earthMat, earthFix);
	earth->m_mesh = LoadMeshFromFile("bigSphere.obj");

	astroObj* earthAstro = createAstroObj(earth, sunAstro, 5.972 * pow(10, 24), 6371, 365.256363004, 0.0167086);

	/**
	*
	*	MOON
	*
	**/
	Mat4 moonMat = Identity4();
	moonMat.data[2][3] = -2.5;
	moonMat.data[0][3] = 2.0;
	Object* moonRot = CreateObject(earthMat, sunFix);
	Object* moonFix = CreateObject(moonMat, moonRot);

	for (int i = 0; i < 3; i++)
		moonMat.data[i][i] = 0.3;
	Object* moon = CreateObject(moonMat, moonFix);
	moon->m_mesh = LoadMeshFromFile("smallSphere.obj");

	astroObj* moonAstro = createAstroObj(moon, earthAstro, 7.348 * pow(10, 22), 1737.1, 365, 0.0549);


	/**
	*
	*	Mars
	*
	**/
	Mat4 marsMat = Identity4();
	marsMat.data[2][3] = -20.0;
	marsMat.data[0][3] = 2.0;
	Object* marsRot = CreateObject(sunMat, root);
	Object* marsFix = CreateObject(marsMat, marsRot);

	for (int i = 0; i < 3; i++)
		marsMat.data[i][i] = 0.5;
	Object* mars = CreateObject(marsMat, marsFix);
	mars->m_mesh = LoadMeshFromFile("bigSphere.obj");

	astroObj* marsAstro = createAstroObj(mars, sunAstro, 6.417 * pow(10, 23), 3389.5, 687, 0.093);

	/**
	*
	*	JUPITER
	*
	**/
	Mat4 jupiterMat = Identity4();
	jupiterMat.data[2][3] = -16.0;
	jupiterMat.data[0][3] = 2.0;
	Object* jupiterRot = CreateObject(sunMat, root);
	Object* jupiterFix = CreateObject(jupiterMat, jupiterRot);

	for (int i = 0; i < 3; i++)
		jupiterMat.data[i][i] = 1.0;
	Object* jupiter = CreateObject(jupiterMat, jupiterFix);
	jupiter->m_mesh = LoadMeshFromFile("bigSphere.obj");

	astroObj* jupiterAstro = createAstroObj(jupiter, sunAstro, 1.899 * pow(10, 27), 69911, 12*365.25, 0.048);

	

	/**
	*
	*	SATURN
	*
	**/
	Mat4 saturnMat = Identity4();
	saturnMat.data[2][3] = -13.0;
	saturnMat.data[0][3] = 2.0;
	Object* saturnRot = CreateObject(sunMat, root);
	Object* saturnFix = CreateObject(saturnMat, saturnRot);

	for (int i = 0; i < 3; i++)
		saturnMat.data[i][i] = 0.8;
	Object* saturn = CreateObject(saturnMat, saturnFix);
	saturn->m_mesh = LoadMeshFromFile("bigSphere.obj");

	astroObj* saturnAstro = createAstroObj(saturn, sunAstro, 5.685 * pow(10, 26), 58232, 29 * 365.25, 0.056);

	/**
	*
	*	URANUS
	*
	**/
	Mat4 uranusMat = Identity4();
	uranusMat.data[2][3] = -10.0;
	uranusMat.data[0][3] = 2.0;
	Object* uranusRot = CreateObject(sunMat, root);
	Object* uranusFix = CreateObject(uranusMat, uranusRot);

	for (int i = 0; i < 3; i++)
		uranusMat.data[i][i] = 0.4;
	Object* uranus = CreateObject(uranusMat, uranusFix);
	uranus->m_mesh = LoadMeshFromFile("bigSphere.obj");

	astroObj* uranusAstro = createAstroObj(uranus, sunAstro, 8.682 * pow(10, 25), 25362, 84 * 365.25, 0.047);

	/**
	*
	*	NEPTUNE
	*
	**/
	Mat4 neptuneMat = Identity4();
	neptuneMat.data[2][3] = -7.0;
	neptuneMat.data[0][3] = 2.0;
	Object* neptuneRot = CreateObject(sunMat, root);
	Object* neptuneFix = CreateObject(neptuneMat, neptuneRot);

	for (int i = 0; i < 3; i++)
		neptuneMat.data[i][i] = 0.4;
	Object* neptune = CreateObject(neptuneMat, neptuneFix);
	neptune->m_mesh = LoadMeshFromFile("bigSphere.obj");

	astroObj* neptuneAstro = createAstroObj(neptune, sunAstro, 1.014 * pow(10, 26), 24622, 165 * 365.25, 0.009);



	// Cr�ation et lancement du timer
	timer = CreateTimer();
	StartTimer(timer);
	displayUIHeader();
	double frames = 0;

	while (!quit)
	{
		UpdateTimer(timer);

		while (SDL_PollEvent(&e))
		{
			switch (e.type)
			{
			case SDL_QUIT:
				quit = 1;
				break;

			case SDL_KEYDOWN:
				if (e.key.keysym.sym == SDLK_ESCAPE)
				{
					quit = 1;
					break;
				}
				
				break;

			case SDL_MOUSEBUTTONDOWN:
				mouseButton = SDL_GetMouseState(&mousePos.m_x, &mousePos.m_y);

				break;
			}
		}

		// On vide la fen�tre (on la remplit en noir)
		SDL_FillRect(window, &(window->clip_rect), SDL_MapRGB(window->format, 0, 0, 0));

		//YRotate(sun, rotationSpeed, sunFix);
		YRotate(mercury, rotationSpeed, mercuryFix);
		YRotate(venus, rotationSpeed, venusFix);
		YRotate(earth, rotationSpeed, earthFix);
		YRotate(moon, rotationSpeed, moonFix);
		YRotate(mars, rotationSpeed, marsFix);
		YRotate(jupiter, rotationSpeed, jupiterFix);
		YRotate(saturn, rotationSpeed, saturnFix);
		YRotate(uranus, rotationSpeed, uranusFix);
		YRotate(neptune, rotationSpeed, neptuneFix);

		frames++;
		displayAstroParameters(earthAstro, timer, frames);
		
		//updateAstro(mercuryAstro, timer);
		//updateAstro(venusAstro, timer);
		updateAstro(earthAstro, frames);
		//updateAstro(moonAstro, timer);
		//updateAstro(marsAstro, timer);
		//updateAstro(jupiterAstro, timer);
		//updateAstro(saturnAstro, timer);
		//updateAstro(neptuneAstro, timer);
		//updateAstro(uranusAstro, timer);


		RenderImage(cam, root, window);

		// Met � jour du buffer de la fen�tre
		SDL_UpdateRect(window, 0, 0, 0, 0);
		// Flip le buffer pour l'envoyer vers l'�cran (la fen�tre est configur�e en double buffer)
		SDL_Flip(window);
	}

	return EXIT_SUCCESS;
}