#pragma once

#include "object.h"


typedef struct sCamera
{
	Object *	m_obj;
	Mat4		m_projMatrix;

	double		m_angleOfView;
	double		m_focal;

	double		m_right;
	double		m_left;

	double		m_top;
	double		m_bottom;

	double		m_near;
	double		m_far;

	int			m_resX;
	int			m_resY;
}Camera;

Camera* CreateCamera(double r, double l, double t, double b, double n, double f, int x, int y);

void RenderImage(Camera* p_cam, Object* p_obj, SDL_Surface* p_surface);

Mat4 GetProjectionMatrix(Camera* p_cam);
